//
//  DettaglioVC.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "ViewController.h"
#import "PriceCell.h"

@interface DettaglioVC : UIViewController <UITableViewDelegate, UITableViewDataSource, PriceCellDelegate>

@property (strong, nonatomic) IBOutlet UITableView *detailTableView;

@property NSDictionary* product;

//-(void)updateTable;

@end
