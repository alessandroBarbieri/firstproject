//
//  InfoEspCell.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "InfoEspCell.h"

@implementation InfoEspCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSString*)string {
    self.infoEspLabel.text = string;
    
    self.infoEspView.clipsToBounds = YES;
    self.infoEspView.layer.cornerRadius = 8;
}

@end
