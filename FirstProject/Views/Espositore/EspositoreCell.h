//
//  EspositoreCell.h
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EspositoreCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *productEspImg;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productDescription;
@property (strong, nonatomic) IBOutlet UIButton *btnBadge;


-(void)configure:(NSDictionary*)elem;
-(void)configureFirstCell;

@end
