//
//  InfoEspCell.h
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoEspCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *infoEspLabel;
@property (strong, nonatomic) IBOutlet UIView *infoEspView;

-(void)configure:(NSString*)string;

@end
