//
//  EspositoreCell.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "EspositoreCell.h"

@implementation EspositoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureFirstCell {
    self.productEspImg.image = [UIImage imageNamed:@"scarico_green_icon"];
    self.productName.text = @"Scarichi";
    self.productDescription.text = @"Descrizione";
    self.btnBadge.hidden = YES;
    //self.badge.text = @"";
}

-(void)configure:(NSDictionary*)elem {
    self.productEspImg.image = [elem objectForKey:@"ImgProdotto"];
    self.productName.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"NomeProdotto"]];
    self.productDescription.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"Descrizione"]];
    [self.btnBadge setTitle:[NSString stringWithFormat:@"%@", [elem objectForKey:@"Badge"]] forState:UIControlStateNormal];
    //self.badge.text = [NSString stringWithFormat:@"%@", [elem objectForKey:@"Badge"]];
    self.btnBadge.layer.borderWidth = 1.0;
    self.btnBadge.clipsToBounds = YES;
    self.btnBadge.layer.cornerRadius = 8;
    CGFloat red = 233.0f/255.0f;
    CGFloat green = 71.0f/255.0f;
    CGFloat blue = 75.0f/255.0f;
    self.btnBadge.layer.borderColor = [[UIColor colorWithRed:red green:green blue:blue alpha:1.0f] CGColor];
}

@end
