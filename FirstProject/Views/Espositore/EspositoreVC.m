//
//  EspositoreVC.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "EspositoreVC.h"
#import "InfoEspCell.h"
#import "EspositoreCell.h"

@interface EspositoreVC ()

@end

@implementation EspositoreVC{
    NSArray* newList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    newList = @[
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"203",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Batterie"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"1167",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Pastiglie freni"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"85",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Olio Motore"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"102",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Filtri"
                    }
                ];
    
    [self.espTableView registerNib:[UINib nibWithNibName:NSStringFromClass([InfoEspCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InfoEspCell class])];
    
    [self.espTableView registerNib:[UINib nibWithNibName:NSStringFromClass([EspositoreCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([EspositoreCell class])];
    
    
    self.espTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0)
        return 1;
    return [newList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0){
        //tipo uno di cella
        InfoEspCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InfoEspCell class]) forIndexPath:indexPath];
        [cell configure:@"45,50 € rimanenti per avere la spedizione gratuita dei prodotti da espositore"];
        return cell;
        
    } else {
        // tipo due
        EspositoreCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EspositoreCell class]) forIndexPath:indexPath];
        if(indexPath.row == 0) {
            [cell configureFirstCell];
        } else {
            [cell configure:newList[indexPath.row - 1]];
        }
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        return 90;
    } else {
        return 85;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
