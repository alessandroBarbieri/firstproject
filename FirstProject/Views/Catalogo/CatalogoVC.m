//
//  CatalogoVC.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "CatalogoVC.h"
#import "CatalogoCell.h"
#import "EspositoreCell.h"

@interface CatalogoVC ()

@end

@implementation CatalogoVC{
    NSArray* newList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    newList = @[
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"203",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Batterie"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"1167",
                    @"Descrizione": @"Descrizione Prodotto ahusahbfuahfuagufgurawfguaerggufhuaehvuhesdftdhf",
                    @"NomeProdotto": @"Pastiglie freni"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"85",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Olio Motore"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"102",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Filtri"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"98",
                    @"Descrizione": @"Descrizione Prodotto blablabberbuniiijoblaa ciao ciao ciao ciao ciao ciao!",
                    @"NomeProdotto": @"Fusibili"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"2102",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Lampadine"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"104",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Cristalli"
                    },
                @{
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Badge": @"60030",
                    @"Descrizione": @"Descrizione Prodotto",
                    @"NomeProdotto": @"Sospensioni"
                    }
                ];
    
    [self.catalogoTableView registerNib:[UINib nibWithNibName:NSStringFromClass([CatalogoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([CatalogoCell class])];
    
    [self.catalogoTableView registerNib:[UINib nibWithNibName:NSStringFromClass([EspositoreCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([EspositoreCell class])];
    
    self.catalogoTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else {
        return [newList count];
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        
        NSDictionary* banner = @{
                                 @"bannerImg" : [UIImage imageNamed:@"logo_login"],
                                 @"bannerTitle" : @"PAZZESCO! 50+40% di sconto!",
                                 @"bannerDescription" : @"Sulle pastiglie freni elencate in tabella. Combo Box Stahlwille in OMAGGIO!",
                                 @"bannerTime" : @"Scade tra 5 giorni! AFFRETTATEVI!"
                                 };
        
        CatalogoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CatalogoCell class]) forIndexPath:indexPath];
        [cell configure:banner];
        return cell;
    } else {
        EspositoreCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([EspositoreCell class]) forIndexPath:indexPath];
        [cell configure:newList[indexPath.row]];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0) {
        return 132;
    } else {
        return 85;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
