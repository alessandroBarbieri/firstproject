//
//  CatalogoCell.m
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "CatalogoCell.h"

@implementation CatalogoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.promoImg.image = [elem objectForKey:@"bannerImg"];
    self.promoTitle.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"bannerTitle"]];
    self.promoDescription.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"bannerDescription"]];
    self.promoTime.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"bannerTime"]];
    
    self.btnPromo.clipsToBounds = YES;
    self.btnPromo.layer.cornerRadius = 8;
    
    self.promoImg.clipsToBounds = YES;
    self.promoImg.layer.cornerRadius = 27.5;
    
    UIImage *image = [UIImage imageNamed:@"info_black_icon"];
    self.infoImg.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.infoImg setTintColor:[UIColor colorWithRed:140.0f/255.0f green:140.0f/255.0f blue:140.0f/255.0f alpha:1.0f]];

}

@end
