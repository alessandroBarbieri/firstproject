//
//  SearchTargaVC.h
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTargaVC : UIViewController<UITextViewDelegate>
@property (strong, nonatomic) IBOutlet UITextView *targa1;
@property (strong, nonatomic) IBOutlet UITextView *targa2;
@property (strong, nonatomic) IBOutlet UITextView *targa3;
@property (strong, nonatomic) IBOutlet UITextView *targa4;
@property (strong, nonatomic) IBOutlet UITextView *targa5;
@property (strong, nonatomic) IBOutlet UITextView *targa6;
@property (strong, nonatomic) IBOutlet UITextView *targa7;
@property (strong, nonatomic) IBOutlet UIButton *btnConferma;
@property (strong, nonatomic) IBOutlet UIView *targaView;

-(void)adjustContentSize:(UITextView*)targa;

@end
