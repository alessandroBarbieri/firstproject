//
//  SearchManualVC.m
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchManualVC.h"

@interface SearchManualVC ()

@end

@implementation SearchManualVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnConferma.clipsToBounds = YES;
    self.btnConferma.layer.cornerRadius = 18;
    
    self.codeEANView.clipsToBounds = YES;
    self.codeEANView.layer.cornerRadius = 8;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.title = @"DIGITA CODICE EAN";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
