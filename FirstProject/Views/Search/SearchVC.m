//
//  SearchVC.m
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchVC.h"
#import "SearchCell.h"
#import "SearchManualVC.h"
#import "SearchTargaVC.h"

@interface SearchVC ()

@end

@implementation SearchVC {
    NSArray* ricercaTarga;
    NSArray* ricercaEAN;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ricercaTarga = @[
                @{
                    @"searchImg": [UIImage imageNamed:@"scan_targa_red_icon"],
                    @"searchTitle": @"Scansiona Targa",
                    @"searchDescription": @"Utilizza la fotocamera per ottenere le info sul veicolo"
                    },
                @{
                    @"searchImg": [UIImage imageNamed:@"insert_targa_black_icon"],
                    @"searchTitle": @"Digita Targa",
                    @"searchDescription": @"Inserisci manualmente il num. di targa del veicolo"
                    }
                ];
    
    ricercaEAN = @[
                     @{
                         @"searchImg": [UIImage imageNamed:@"scan_barcode_red_icon"],
                         @"searchTitle": @"Scansiona EAN",
                         @"searchDescription": @"Utilizza la fotocamera come un lettore barcode"
                         },
                     @{
                         @"searchImg": [UIImage imageNamed:@"insert_code_black_icon"],
                         @"searchTitle": @"Digita EAN",
                         @"searchDescription": @"Inserisci manualmente il cod.EAN relativo al prodotto"
                         }
                     ];
    
    [self.searchTableView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SearchCell class])];
    
    self.searchTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    /** back image nav bar **/
    UIImage *backButtonImage = [[UIImage imageNamed:@"back_white_icon-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationController.navigationBar.backIndicatorImage = backButtonImage;
    self.navigationController.navigationBar.backIndicatorTransitionMaskImage = backButtonImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.title = @"RICERCA";
}

-(void)viewWillDisappear:(BOOL)animated{
    //self.title = @" ";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 1) {
        return [ricercaTarga count];
    } else if(section == 3) {
        return [ricercaEAN count];
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        NSDictionary* search = @{
                                 @"searchImg" : [UIImage imageNamed:@"insert_code_black_icon"],
                                 @"searchTitle" : @"Digita codice prodotto",
                                 @"searchDescription" : @"Inserisci manualmente il cod. prodotto BRC o altro marchio"
                                 };
        SearchCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchCell class]) forIndexPath:indexPath];
        [cell configure:search];
        return cell;
    } else if(indexPath.section == 1){
        SearchCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchCell class]) forIndexPath:indexPath];
        [cell configure:ricercaTarga[(indexPath.row)]];
        return cell;
    } else if(indexPath.section == 2){
        NSDictionary* search = @{
                                 @"searchImg" : [UIImage imageNamed:@"veicolo_black_icon"],
                                 @"searchTitle" : @"Ricerca manuale",
                                 @"searchDescription" : @"Inserisci il nome del veicolo per trovare i prodotti adatti"
                                 };
        SearchCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchCell class]) forIndexPath:indexPath];
        [cell configure:search];
        return cell;
    } else {
        SearchCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchCell class]) forIndexPath:indexPath];
        [cell configure:ricercaEAN[(indexPath.row)]];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section==3 && indexPath.row==1){
        SearchManualVC *searchEAN = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SearchManualVC class])];
        [self.navigationController pushViewController:searchEAN animated:YES];
    } else if(indexPath.section==1 && indexPath.row==1){
        SearchTargaVC *targa = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([SearchTargaVC class])];
        [self.navigationController pushViewController:targa animated:YES];
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    viewHeader.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f];

    CGRect frame = CGRectMake(20, 24, viewHeader.frame.size.width-20, 10);
    
    UILabel* lbl = [[UILabel alloc] initWithFrame:frame];
    [lbl setTextColor:[UIColor colorWithRed:104.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0f]];
    [lbl setFont:[UIFont systemFontOfSize:14 weight:UIFontWeightMedium]];
    
    lbl.textAlignment = NSTextAlignmentLeft;
    if(section == 0) {
        lbl.text = @"RICERCA PER CODICE";
    } else if(section == 1) {
        lbl.text = @"RICERCA PER TARGA (50 utilizzi residui)";
    } else if(section == 2) {
        lbl.text = @"RICERCA PER VEICOLO";
    } else {
        lbl.text = @"RICERCA PER  CODICE EAN";
    }
    
    [viewHeader addSubview:lbl];
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"DATA!";
}*/

/*- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
 
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    UIView* viewHeader = [[UIView alloc] init];
    viewHeader.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f];
    [header setBackgroundView:viewHeader];
    header.textLabel.textColor = [UIColor colorWithRed:104.0f/255.0f green:103.0f/255.0f blue:103.0f/255.0f alpha:1.0f];
    header.textLabel.font = [UIFont boldSystemFontOfSize:14];
    CGRect headerFrame = header.frame;
    header.textLabel.frame = headerFrame;
    header.textLabel.textAlignment = NSTextAlignmentLeft;
    if(section == 0) {
        header.textLabel.text = @"RICERCA PER CODICE";
    } else if(section == 1) {
        header.textLabel.text = @"RICERCA PER TARGA (50 utilizzi residui)";
    } else if(section == 2) {
        header.textLabel.text = @"RICERCA PER VEICOLO";
    } else {
        header.textLabel.text = @"RICERCA PER  CODICE EAN";
    }
}*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
@end
