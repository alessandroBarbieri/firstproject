//
//  SearchManualVC.h
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchManualVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *codeEAN;
@property (strong, nonatomic) IBOutlet UIButton *btnConferma;
@property (strong, nonatomic) IBOutlet UIView *codeEANView;

@end
