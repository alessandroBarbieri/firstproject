//
//  SearchTargaVC.m
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SearchTargaVC.h"

@interface SearchTargaVC ()

@end

@implementation SearchTargaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnConferma.clipsToBounds = YES;
    self.btnConferma.layer.cornerRadius = 18;
    
    self.targaView.clipsToBounds = YES;
    self.targaView.layer.cornerRadius = 8;
    
    self.targa1.delegate = self;
    self.targa1.layer.borderWidth = 1;
    self.targa1.clipsToBounds = YES;
    self.targa1.layer.cornerRadius = 4;
    self.targa1.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa1];
    
    self.targa2.delegate = self;
    self.targa2.layer.borderWidth = 1;
    self.targa2.clipsToBounds = YES;
    self.targa2.layer.cornerRadius = 4;
    self.targa2.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa2];
    
    self.targa3.delegate = self;
    self.targa3.layer.borderWidth = 1;
    self.targa3.clipsToBounds = YES;
    self.targa3.layer.cornerRadius = 4;
    self.targa3.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa3];
    
    self.targa4.delegate = self;
    self.targa4.layer.borderWidth = 1;
    self.targa4.clipsToBounds = YES;
    self.targa4.layer.cornerRadius = 4;
    self.targa4.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa4];
    
    self.targa5.delegate = self;
    self.targa5.layer.borderWidth = 1;
    self.targa5.clipsToBounds = YES;
    self.targa5.layer.cornerRadius = 4;
    self.targa5.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa5];
    
    self.targa6.delegate = self;
    self.targa6.layer.borderWidth = 1;
    self.targa6.clipsToBounds = YES;
    self.targa6.layer.cornerRadius = 4;
    self.targa6.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa6];
    
    self.targa7.delegate = self;
    self.targa7.layer.borderWidth = 1;
    self.targa7.clipsToBounds = YES;
    self.targa7.layer.cornerRadius = 4;
    self.targa7.layer.borderColor = [UIColor colorWithRed:200.0f/255.0f green:199.0f/255.0f blue:204.0f/255.0f alpha:1.0].CGColor;
    [self adjustContentSize:self.targa7];
    
    [self.targa1 becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.title = @"DIGITA TARGA VEICOLO";
}

-(void)adjustContentSize:(UITextView*)targa{
    CGFloat deadSpace = ([targa bounds].size.height - [targa contentSize].height);
    CGFloat inset = MAX(0, deadSpace/2.0);
    targa.contentInset = UIEdgeInsetsMake(inset, targa.contentInset.left, inset, targa.contentInset.right);
}

-(void)textViewDidChange:(UITextView *)textView{
    [self goToNextResponder:textView];
}

-(void)goToNextResponder:(UITextView *)textView{
    
    NSString *pattern = @"[a-zA-Z]{1,1}";

    
    if(textView == self.targa1 && self.targa1.text.length == 1){
        self.targa1.text = [self.targa1.text uppercaseString];
        NSError  *error = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSUInteger matches = [regex numberOfMatchesInString:self.targa1.text options:0 range:NSMakeRange(0,[self.targa1.text length])];
        if(matches > 0){
            [self.targa2 becomeFirstResponder];
        }else{
            self.targa1.text = @"";
        }
    }else if(textView == self.targa2  && self.targa2.text.length == 1){
        self.targa2.text = [self.targa2.text uppercaseString];
        NSError  *error = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSUInteger matches = [regex numberOfMatchesInString:self.targa2.text options:0 range:NSMakeRange(0,[self.targa2.text length])];
        if(matches > 0){
            [self.targa3 becomeFirstResponder];
        }else{
            self.targa2.text = @"";
        }
        //[self.targa3 becomeFirstResponder];
    }else if(textView == self.targa3  && self.targa3.text.length == 1){
        if([self.targa3.text isEqualToString:@","]) {
            self.targa3.text = @"";
        } else {
            [self.targa4 becomeFirstResponder];
        }
    }else if(textView == self.targa4  && self.targa4.text.length == 1){
        if([self.targa4.text isEqualToString:@","]) {
            self.targa4.text = @"";
        } else {
            [self.targa5 becomeFirstResponder];
        }
    }else if(textView == self.targa5  && self.targa5.text.length == 1){
        if([self.targa5.text isEqualToString:@","]) {
            self.targa5.text = @"";
        } else {
            [self.targa6 becomeFirstResponder];
        }
    }else if(textView == self.targa6  && self.targa6.text.length == 1){
        self.targa6.text = [self.targa6.text uppercaseString];
        NSError  *error = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSUInteger matches = [regex numberOfMatchesInString:self.targa6.text options:0 range:NSMakeRange(0,[self.targa6.text length])];
        if(matches > 0){
            [self.targa7 becomeFirstResponder];
        }else{
            self.targa6.text = @"";
        }
    }else if(textView == self.targa7  && self.targa7.text.length == 1){
        self.targa7.text = [self.targa7.text uppercaseString];
        NSError  *error = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern: pattern options:0 error:&error];
        NSUInteger matches = [regex numberOfMatchesInString:self.targa7.text options:0 range:NSMakeRange(0,[self.targa7.text length])];
        if(matches > 0){
            //[self.targa3 becomeFirstResponder];
        }else{
            self.targa7.text = @"";
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
