//
//  infoCell.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface infoCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *detailName;
@property (strong, nonatomic) IBOutlet UILabel *detailInfo;

-(void)configure:(NSDictionary*)elem index:(NSInteger)row;

@end
