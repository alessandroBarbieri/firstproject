//
//  infoCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "infoCell.h"

@implementation infoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem index:(NSInteger)row {
    
    self.detailName.text = @"Posizione";
    self.detailInfo.text = [elem objectForKey:@"NomeProdotto"];
    
    switch (row) {
        case 0:
            self.detailName.text = @"Posizione:";
            self.detailInfo.text = [elem objectForKey:@"Posizione"];
            self.detailInfo.textColor = [UIColor colorWithRed:243.0/255.0f green:162.0/255.0f blue:67.0/255.0f alpha:1];
            break;
        case 1:
            self.detailName.text = @"Categoria:";
            self.detailInfo.text = [elem objectForKey:@"Categoria"];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 2:
            self.detailName.text = @"Codice:";
            self.detailInfo.text = [elem objectForKey:@"CodArticolo"];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 3:
            self.detailName.text = @"EAN:";
            self.detailInfo.text = [elem objectForKey:@"EAN"];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 4:
            self.detailName.text = @"Consegna:";
            self.detailInfo.text = [NSString stringWithFormat:@"%@ ore",[elem objectForKey:@"TempoConsegna"]];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        case 5: {
            self.detailName.text = @"Scontistica:";
            NSString* full_price = @"26,90";
            
            self.detailInfo.text = [NSString stringWithFormat:@"(%@) -%@",full_price, [elem objectForKey:@"Sconto"]];
            NSMutableAttributedString* string = [[NSMutableAttributedString alloc] initWithAttributedString:self.detailInfo.attributedText];
            [string addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:104.0/255.0f green:103.0/255.0f blue:103.0/255.0f alpha:1] range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", full_price]]];
            [string addAttribute:NSStrikethroughStyleAttributeName value:@1 range:[self.detailInfo.text rangeOfString:[NSString stringWithFormat:@"(%@)", full_price]]];
            
            [self.detailInfo setAttributedText:string];
            break;
        }
        case 6: {
            self.detailName.text = @"Quantità ordinata:";
            self.detailInfo.text = [elem objectForKey:@"QtaOrder"];
            self.detailInfo.textColor = [UIColor blackColor];
            break;
        }
        case 7: {
            self.detailName.text = @"Totale Ordine:";
            self.detailInfo.text = [NSString stringWithFormat:@"€ %0.2f",[[elem objectForKey:@"QtaOrder"] intValue]*[[elem objectForKey:@"PrzUnitario"] floatValue]];
        }
        default:
            break;
    }
    
}

@end
