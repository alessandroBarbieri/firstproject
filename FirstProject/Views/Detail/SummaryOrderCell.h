//
//  SummaryOrderCell.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SummaryOrderCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *summaryImage;
@property (strong, nonatomic) IBOutlet UILabel *summaryNumber;
@property (strong, nonatomic) IBOutlet UILabel *summaryPrice;
@property (strong, nonatomic) IBOutlet UILabel *summaryStatus;
@property (strong, nonatomic) IBOutlet UIView *viewContent;

-(void)configure:(NSDictionary*)elem;

@end
