//
//  DetailCell.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *detailImage;
@property (strong, nonatomic) IBOutlet UIButton *detailCarrello;
@property (strong, nonatomic) IBOutlet UILabel *detailService;
@property (strong, nonatomic) IBOutlet UILabel *detailProduct;
@property (strong, nonatomic) IBOutlet UILabel *detailPrice;

-(void)configure:(NSDictionary*)elem;

@end
