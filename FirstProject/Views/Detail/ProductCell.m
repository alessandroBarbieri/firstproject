//
//  ProductCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem{
    
    self.btnCatalogo.clipsToBounds = YES;
    self.btnCatalogo.layer.cornerRadius = 8;
    
    if(self.isOrder == YES){
        //nascondi icona cestino
    }
    
    self.productName.text = [elem objectForKey:@"NomeProdotto"];
    self.productCode.text = [@"Codice: " stringByAppendingString:[elem objectForKey:@"CodArticolo"]];
    self.productEAN.text = [@"EAN: " stringByAppendingString:[elem objectForKey:@"EAN"]];
    self.productPrice.text = [NSString stringWithFormat:@"€ %@ (%@ pezzi)",[elem objectForKey:@"PrzUnitario"],[elem objectForKey:@"QtaSpedita"]];
    self.productImage = [elem objectForKey:@"ImgProdotto"];
    
    self.preferitiImg.hidden = YES;
}

-(void)configurePreferiti:(NSDictionary*)elem {
    self.btnCatalogo.clipsToBounds = YES;
    self.btnCatalogo.layer.cornerRadius = 8;
    
    if(self.isOrder == YES){
        //nascondi icona cestino
    }
    
    self.productName.text = [elem objectForKey:@"NomeProdotto"];
    self.productCode.text = [@"Codice: " stringByAppendingString:[elem objectForKey:@"CodArticolo"]];
    self.productEAN.text = [@"EAN: " stringByAppendingString:[elem objectForKey:@"EAN"]];
    self.productPrice.text = [NSString stringWithFormat:@"€ %@",[elem objectForKey:@"PrzUnitario"]];
    self.productImage = [elem objectForKey:@"ImgProdotto"];
    [self.btnImg setImage:[UIImage imageNamed:@"cart_red_icon"] forState:UIControlStateNormal];
}

@end
