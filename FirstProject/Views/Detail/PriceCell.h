//
//  PriceCell.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PriceCellDelegate;

@interface PriceCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UITextView *detailQuantity;
@property (strong, nonatomic) IBOutlet UIButton *detailShop;
@property (strong, nonatomic) IBOutlet UIStepper *detailStepper;
@property (strong, nonatomic) id<PriceCellDelegate> delegate;

@property (strong, nonatomic) NSDictionary *product;

-(void)configure:(NSDictionary*)elem;
- (IBAction)btnCnt:(UIStepper *)sender;


@end


@protocol PriceCellDelegate

-(void)priceUpdated:(double )quantity;

@end
