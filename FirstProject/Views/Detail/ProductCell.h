//
//  ProductCell.h
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnCatalogo;
@property (strong, nonatomic) IBOutlet UILabel *productName;
@property (strong, nonatomic) IBOutlet UILabel *productCode;
@property (strong, nonatomic) IBOutlet UILabel *productEAN;
@property (strong, nonatomic) IBOutlet UILabel *productPrice;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UIImageView *preferitiImg;
@property (strong, nonatomic) IBOutlet UIButton *btnImg;

@property BOOL isOrder;

-(void)configure:(NSDictionary*)elem;
-(void)configurePreferiti:(NSDictionary*)elem;

@end
