//
//  PriceCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "PriceCell.h"
#import "DettaglioVC.h"

@implementation PriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.detailStepper.wraps = true;
    self.detailStepper.autorepeat = true;
    self.detailStepper.maximumValue = 10;
    self.detailStepper.minimumValue = 1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.product = elem;
    
    self.detailShop.clipsToBounds = YES;
    self.detailShop.layer.cornerRadius = 20;
    
    self.detailQuantity.layer.borderWidth = 1.0;
    self.detailQuantity.clipsToBounds = YES;
    self.detailQuantity.layer.cornerRadius = 4;
    [self.detailStepper setValue:[[elem objectForKey:@"QtaOrder"] doubleValue]];
    self.detailQuantity.text = [NSString stringWithFormat:@"%@", [elem objectForKey:@"QtaOrder"]];
    double num = [[elem objectForKey:@"PrzUnitario"] doubleValue] * [[elem objectForKey:@"QtaOrder"] doubleValue];
    NSLog(@"AGGIORNO CARRELLOOOOOOOOO!!!!!!!!!");
    [self.detailShop setTitle:[NSString stringWithFormat:@"Aggiungi al carrello € %.2f", num] forState:UIControlStateNormal];
   
}

- (IBAction)btnCnt:(UIStepper *)sender {
    double value = [sender value];
    //NSLog(@"STEPPER:    %d",(int)value);
    //self.detailQuantity.text = [NSString stringWithFormat:@"%d", (int)value];
    
    //self.detailShop.titleLabel.text = [NSString stringWithFormat:@"Aggiungi al carrello € %f", num];
    //DettaglioVC* detailVC =[[DettaglioVC alloc]init];
    //[detailVC updateTable];

    NSMutableDictionary* _mut = self.product.mutableCopy;
    [_mut setObject:@(value) forKey:@"QtaOrder"];
    self.product = [[NSDictionary alloc] initWithDictionary:_mut];
    [self.delegate priceUpdated:value];
    //_product;
}

@end
