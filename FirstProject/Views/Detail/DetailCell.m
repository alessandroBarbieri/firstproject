//
//  DetailCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailCell.h"

@implementation DetailCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    
    if([[elem objectForKey:@"Carrello"] isEqualToString:@"NO"]) {
        self.detailCarrello.hidden = YES;
    } else {
        self.detailCarrello.clipsToBounds = YES;
        self.detailCarrello.layer.cornerRadius = 30;
    }
    
    self.detailService.text = @"BRC car service";
    self.detailProduct.text = [elem objectForKey:@"NomeProdotto"];
    double price = [[elem objectForKey:@"PrzUnitario"] doubleValue]; //* [[elem objectForKey:@"QtaOrder"] doubleValue];
    self.detailPrice.text = [NSString stringWithFormat:@"€ %.02f",price]; 
    self.detailImage.image = [elem objectForKey:@"ImgProdotto"];
    
    /*if(self.isOrder == YES){
        //nascondi icona cestino
    }
    
    self.productName.text = [elem objectForKey:@"NomeProdotto"];
    self.productCode.text = [@"Codice: " stringByAppendingString:[elem objectForKey:@"CodArticolo"]];
    self.productEAN.text = [@"EAN: " stringByAppendingString:[elem objectForKey:@"EAN"]];
    self.productPrice.text = [NSString stringWithFormat:@"€ %@ (%@ pezzi)",[elem objectForKey:@"PrzUnitario"],[elem objectForKey:@"QtaSpedita"]];
    self.productImage = [elem objectForKey:@"ImgProdotto"];*/
}


@end
