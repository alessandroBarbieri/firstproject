//
//  SummaryOrderCell.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "SummaryOrderCell.h"

@implementation SummaryOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem{
    
    self.viewContent.clipsToBounds = YES;
    self.viewContent.layer.cornerRadius = 8;
    
    self.summaryPrice.text = [NSString stringWithFormat:@"€ %@ - %@ prodotti - %@", [elem objectForKey:@"Prezzo"], [elem objectForKey:@"QtaSpedita"], [elem objectForKey:@"DataConsegna"]];
    self.summaryNumber.text = [elem objectForKey:@"NumOrder"];
    self.summaryStatus.text = [@"Status: " stringByAppendingString: [elem objectForKey:@"Stato"]];
    self.summaryImage.image = [elem objectForKey:@"ImgStatus2"];
    
    /*@"QtaOrder": @"5",
    @"Prezzo": @"125.90",
    @"NumOrder": @"ORDINE 000001",
    @"ImgStatus": [UIImage imageNamed:@"status_black_icon"],
    @"Stato": @"ACCETTATO",
    //@"TempoConsegna": @"24",
    @"DataConsegna": @"16/06/16",
    @"CodArticolo": @"BRC1401",
    @"QtaSpedita": @"2",
    //@"PrzUnitario": @"25.90",
    @"EAN": @"1234567890123",
    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
    @"Posizione": @"CATALOGO",
    @"Sconto": @"50%",
    @"TempoConsegna": @"24",
    @"Categoria": @"Pastiglie Freni",
    @"NomeProdotto": @"Pastiglie freni A"*/
    
}

@end
