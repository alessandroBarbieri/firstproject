//
//  DatiUtenteVC.m
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DatiUtenteVC.h"
#import "UserCell.h"
#import "InfoUserCell.h"
#import "AssistenzaVC.h"
#import "DocumentazioneCell.h"
#import "ListaPreferitiVC.h"

@interface DatiUtenteVC ()

@end

@implementation DatiUtenteVC{
    NSArray* newList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    newList = @[
                @{
                    @"infoImg": [UIImage imageNamed:@"star_outline_black_icon"],
                    @"info": @"Lista preferiti"
                    },
                @{
                    @"infoImg": [UIImage imageNamed:@"info_black_icon"],
                    @"info": @"Assistenza"
                    }
                ];
    
    [self.userTableView registerNib:[UINib nibWithNibName:NSStringFromClass([UserCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([UserCell class])];
    
    [self.userTableView registerNib:[UINib nibWithNibName:NSStringFromClass([InfoUserCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([InfoUserCell class])];
    
    [self.userTableView registerNib:[UINib nibWithNibName:NSStringFromClass([DocumentazioneCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DocumentazioneCell class])];
    
    self.btnLogout.clipsToBounds = YES;
    self.btnLogout.layer.cornerRadius = 22;
    
    self.userTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    /** back image nav bar **/
    UIImage *backButtonImage = [[UIImage imageNamed:@"back_white_icon-1"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationController.navigationBar.backIndicatorImage = backButtonImage;
    self.navigationController.navigationBar.backIndicatorTransitionMaskImage = backButtonImage;
    
}


-(void)viewWillAppear:(BOOL)animated{
    self.title = @"DATI UTENTE";
}

-(void)viewWillDisappear:(BOOL)animated{
    //self.title = @" ";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBack:(id)sender {
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else if(section == 1) {
        return [newList count];
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        
        NSDictionary* user = @{
                                 @"UserImg": [UIImage imageNamed:@"user_profile_red_icon"],
                                 @"UserName": @"Officina Name",
                                 @"UserMail": @"officina@gmail.com"
                                 };
        
        UserCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([UserCell class]) forIndexPath:indexPath];
        [cell configure:user];
        return cell;
    } else if(indexPath.section == 1){
        InfoUserCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([InfoUserCell class]) forIndexPath:indexPath];
        [cell configure:newList[indexPath.row]];
        return cell;
    } else {
        DocumentazioneCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DocumentazioneCell class]) forIndexPath:indexPath];
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section==1 && indexPath.row==1){
        AssistenzaVC *assistenza = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([AssistenzaVC class])];
        [self.navigationController pushViewController:assistenza animated:YES];
    } else if(indexPath.section==1 && indexPath.row==0){
        ListaPreferitiVC *assistenza = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ListaPreferitiVC class])];
        [self.navigationController pushViewController:assistenza animated:YES];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0) {
        return 197;
    } else if(indexPath.section == 1){
        return 52;
    } else {
        return 60;
    }
}

@end
