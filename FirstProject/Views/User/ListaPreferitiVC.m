//
//  ListaPreferitiVC.m
//  FirstProject
//
//  Created by Etinet on 11/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "ListaPreferitiVC.h"
#import "ProductCell.h"

@interface ListaPreferitiVC ()

@end

@implementation ListaPreferitiVC{
    NSArray* newList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    newList = @[
                @{
                    //@"IDOrdine": @"A123    ",
                    //@"Mail": @"",
                    //@"CodCliente": @0,
                    //@"CodBreve": @128777,
                    @"CodArticolo": @"BRC1401",
                    //@"QtaOriginale": @2,
                    @"QtaSpedita": @"2",
                    @"QtaOrder": @"1",
                    @"PrzUnitario": @"25.90",
                    @"EAN": @"1234567890123",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"50%",
                    @"TempoConsegna": @"24",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni A"
                    //@"DataPromessa": @"2017-12-20T00:00:00",
                    //@"DataSpedizione": @"2017-12-20T00:00:00",
                    //@"NOrdineAntecs": @"           ",
                    //@"NRigaAntecs": @0,
                    //@"NBolla": @"1300    ",
                    //@"Stato": @"4 - Spedito"
                    },
                @{
                    @"CodArticolo": @"BRC8932",
                    @"QtaSpedita": @"2",
                    @"QtaOrder": @"2",
                    @"PrzUnitario": @"13.80",
                    @"EAN": @"1325467890321",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"30%",
                    @"TempoConsegna": @"36",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni G"
                    },
                @{
                    @"CodArticolo": @"BRC6634",
                    @"QtaSpedita": @"1",
                    @"QtaOrder": @"1",
                    @"PrzUnitario": @"15.20",
                    @"EAN": @"8734467850081",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"45%",
                    @"TempoConsegna": @"12",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni X"
                    },
                @{
                    @"CodArticolo": @"BRC9022",
                    @"QtaSpedita": @"3",
                    @"QtaOrder": @"2",
                    @"PrzUnitario": @"21.30",
                    @"EAN": @"2534439000677",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"60%",
                    @"TempoConsegna": @"48",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni K"
                    }
                ];
    
    [self.preferitiTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ProductCell class])];
    
    self.preferitiTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.title = @"LISTA PREFERITI";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [newList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProductCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ProductCell class]) forIndexPath:indexPath];
    [cell configurePreferiti:newList[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 98;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
