//
//  DatiUtenteVC.h
//  FirstProject
//
//  Created by Etinet on 10/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DatiUtenteVC : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *userTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnLogout;

- (IBAction)onBack:(id)sender;

@end
