//
//  ViewController.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "ViewController.h"
#import "ProductCell.h"
#import "SummaryOrderCell.h"
#import "DettaglioVC.h"
#import "DetailProduct.h"

@interface ViewController ()

@end

@implementation ViewController {
    NSArray* newList;
}

@synthesize order;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    newList = @[
                @{
                    //@"IDOrdine": @"A123    ",
                    //@"Mail": @"",
                    //@"CodCliente": @0,
                    //@"CodBreve": @128777,
                    @"CodArticolo": @"BRC1401",
                    //@"QtaOriginale": @2,
                    @"QtaSpedita": @"2",
                    @"QtaOrder": @"1",
                    @"PrzUnitario": @"25.90",
                    @"EAN": @"1234567890123",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"50%",
                    @"TempoConsegna": @"24",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni A",
                    @"Carrello": @"NO"
                    //@"DataPromessa": @"2017-12-20T00:00:00",
                    //@"DataSpedizione": @"2017-12-20T00:00:00",
                    //@"NOrdineAntecs": @"           ",
                    //@"NRigaAntecs": @0,
                    //@"NBolla": @"1300    ",
                    //@"Stato": @"4 - Spedito"
                    },
                @{
                    @"CodArticolo": @"BRC8932",
                    @"QtaSpedita": @"2",
                    @"QtaOrder": @"2",
                    @"PrzUnitario": @"13.80",
                    @"EAN": @"1325467890321",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"30%",
                    @"TempoConsegna": @"36",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni G",
                    @"Carrello": @"NO"
                    },
                @{
                    @"CodArticolo": @"BRC6634",
                    @"QtaSpedita": @"1",
                    @"QtaOrder": @"1",
                    @"PrzUnitario": @"15.20",
                    @"EAN": @"8734467850081",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"45%",
                    @"TempoConsegna": @"12",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni X",
                    @"Carrello": @"NO"
                    },
                @{
                    @"CodArticolo": @"BRC9022",
                    @"QtaSpedita": @"3",
                    @"QtaOrder": @"2",
                    @"PrzUnitario": @"21.30",
                    @"EAN": @"2534439000677",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"60%",
                    @"TempoConsegna": @"48",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni K",
                    @"Carrello": @"NO"
                    }
                ];
    
    [self.ordersTableView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([ProductCell class])];
    
    [self.ordersTableView registerNib:[UINib nibWithNibName:NSStringFromClass([SummaryOrderCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SummaryOrderCell class])];
    
    
    self.ordersTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.title = [order objectForKey:@"NumOrder"];
}

-(void)viewWillDisappear:(BOOL)animated{
    //self.title = @" ";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) return 1;
    return [newList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0){
        //tipo uno di cella
        SummaryOrderCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SummaryOrderCell class]) forIndexPath:indexPath];
        /*[cell configure:@{
                          @"price": @"75,00",
                          @"status": @"IN ATTESA",
                          @"order": @"N.0005",
                          @"count": @"4",
                          @"date": @"22/06/2017",
                          @"image": [UIImage imageNamed:@"attesa_grey_icon"]
                          }];*/
        [cell configure: order];
        return cell;
        
    } else {
        // tipo due
        ProductCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ProductCell class]) forIndexPath:indexPath];
        cell.isOrder = YES;
        
        [cell configure:newList[indexPath.row]];
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 1){
        //newList[indexPath.row]
        DetailProduct *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([DetailProduct class])];
        
        detail.product = newList[indexPath.row];
        [self.navigationController pushViewController:detail animated:YES];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        return 115;
    }else {
        return 98;
    }
}

@end
