//
//  DetailProduct.h
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailProduct : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *detailTableView;

@property NSDictionary* product;

@end
