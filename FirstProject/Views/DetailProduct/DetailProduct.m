//
//  DetailProduct.m
//  FirstProject
//
//  Created by Etinet on 14/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DetailProduct.h"
#import "DetailCell.h"
#import "infoCell.h"

@interface DetailProduct ()

@end

@implementation DetailProduct

@synthesize product;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.detailTableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DetailCell class])];
    
    [self.detailTableView registerNib:[UINib nibWithNibName:NSStringFromClass([infoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([infoCell class])];
    
    self.detailTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.detailTableView.estimatedRowHeight = 474;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    //self.title = @"DETTAGLIO PRODOTTO";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else if (section == 1) {
        return 6;
    }
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        //tipo uno di cella
        DetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCell class]) forIndexPath:indexPath];
        [cell configure: product];
        return cell;
    } else if(indexPath.section == 1) {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configure: product index:indexPath.row];
        return cell;
    } else {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configure: product index:indexPath.row+6];
        //cell.delegate = self;
        //NSLog(@"RICARICO CELLAAAAAAAA!!!!!!!!!");
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0) {
        return 474;
    }else if(indexPath.section == 1) {
        return 44;
    } else {
        return 44;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    viewHeader.backgroundColor = [UIColor colorWithRed:232.0f/255.0f green:238.0f/255.0f blue:239.0f/255.0f alpha:1.0f];
    return viewHeader;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 2)
        return 10;
    else
        return 0;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
