//
//  OrdiniVC.h
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "ViewController.h"

@interface OrdiniVC : ViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *ordiniTableView;

@property NSDictionary* product;

@end
