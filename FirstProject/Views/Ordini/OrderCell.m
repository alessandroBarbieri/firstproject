//
//  OrderCell.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "OrderCell.h"

@implementation OrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.orderImg.image = [elem objectForKey:@"ImgStatus"];
    self.orderDate.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"DataConsegna"]];
    self.orderNum.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"NumOrder"]];
    self.orderPrice.text = [NSString stringWithFormat:@"€ %@ - %@ prodotti", [elem objectForKey:@"Prezzo"],[elem objectForKey:@"QtaOrder"]];
    NSString* str = [NSString stringWithFormat:@"%@",[elem objectForKey:@"Stato"]];
    self.orderStatus.text = str;//[NSString stringWithFormat:@"%@",[elem objectForKey:@"Stato"]];
    if([str isEqualToString:@"SPEDITO"]) {
        self.orderStatus.textColor = [UIColor blackColor];
    } else if([str isEqualToString:@"IN ATTESA"]) {
        self.orderStatus.textColor = [UIColor colorWithRed:140.0f/255.0f green:140.0f/255.0f blue:140.0f/255.0f alpha:1.0f];
    } else {
        self.orderStatus.textColor = [UIColor colorWithRed:233.0f/255.0f green:71.0f/255.0f blue:75.0f/255.0f alpha:1.0f];
    }
}

@end
