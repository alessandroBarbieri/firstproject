//
//  OrdiniVC.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "OrdiniVC.h"
#import "OrderCell.h"
#import "ViewController.h"

@interface OrdiniVC ()

@end

@implementation OrdiniVC{
    NSArray* newList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    newList = @[
                @{
                    //@"IDOrdine": @"A123    ",
                    //@"Mail": @"",
                    //@"CodCliente": @0,
                    //@"CodBreve": @128777,
                    //@"CodArticolo": @"BRC1401",
                    //@"QtaOriginale": @2,
                    //@"QtaSpedita": @"2",
                    @"QtaOrder": @"1",
                    @"Prezzo": @"25.90",
                    @"NumOrder": @"ORDINE 000006",
                    //@"EAN": @"1234567890123",
                    @"ImgStatus": [UIImage imageNamed:@"status_black_icon"],
                    @"ImgStatus2": [UIImage imageNamed:@"spedito_black_icon"],
                    //@"Posizione": @"CATALOGO",
                    @"Stato": @"SPEDITO",
                    //@"TempoConsegna": @"24",
                    @"DataConsegna": @"23/12/16",
                    @"CodArticolo": @"BRC5689",
                    @"QtaSpedita": @"1",
                    //@"PrzUnitario": @"25.90",
                    @"EAN": @"3214588890321",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"70%",
                    @"TempoConsegna": @"36",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni X"
                    },
                @{
                    @"QtaOrder": @"4",
                    @"Prezzo": @"75.30",
                    @"NumOrder": @"ORDINE 000003",
                    @"ImgStatus": [UIImage imageNamed:@"status_black_icon"],
                    @"ImgStatus2": [UIImage imageNamed:@"spedito_black_icon"],
                    @"Stato": @"SPEDITO",
                    //@"TempoConsegna": @"12",
                    @"DataConsegna": @"09/02/17",
                    @"CodArticolo": @"BRC2653",
                    @"QtaSpedita": @"3",
                    //@"PrzUnitario": @"25.90",
                    @"EAN": @"6664107852476",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"25%",
                    @"TempoConsegna": @"12",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni C"
                    },
                @{
                    @"QtaOrder": @"2",
                    @"Prezzo": @"10.00",
                    @"NumOrder": @"ORDINE 000009",
                    @"ImgStatus": [UIImage imageNamed:@"status_grey_icon"],
                    @"ImgStatus2": [UIImage imageNamed:@"attesa_grey_icon"],
                    @"Stato": @"IN ATTESA",
                    //@"TempoConsegna": @"24",
                    @"DataConsegna": @"11/09/17",
                    @"CodArticolo": @"BRC7440",
                    @"QtaSpedita": @"2",
                    //@"PrzUnitario": @"25.90",
                    @"EAN": @"1234567890123",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"40%",
                    @"TempoConsegna": @"36",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni Z"
                    },
                @{
                    @"QtaOrder": @"5",
                    @"Prezzo": @"125.90",
                    @"NumOrder": @"ORDINE 000001",
                    @"ImgStatus": [UIImage imageNamed:@"status_red_icon"],
                    @"ImgStatus2": [UIImage imageNamed:@"accettato_red_icon"],
                    @"Stato": @"ACCETTATO",
                    //@"TempoConsegna": @"24",
                    @"DataConsegna": @"16/06/16",
                    @"CodArticolo": @"BRC1401",
                    @"QtaSpedita": @"2",
                    //@"PrzUnitario": @"25.90",
                    @"EAN": @"1234567890123",
                    @"ImgProdotto": [UIImage imageNamed:@"officine_black_icon_nav"],
                    @"Posizione": @"CATALOGO",
                    @"Sconto": @"50%",
                    @"TempoConsegna": @"24",
                    @"Categoria": @"Pastiglie Freni",
                    @"NomeProdotto": @"Pastiglie freni A"
                    }
                ];
    
    [self.ordiniTableView registerNib:[UINib nibWithNibName:NSStringFromClass([OrderCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([OrderCell class])];
    
    self.ordiniTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)viewWillDisappear:(BOOL)animated{
    //self.title = @" ";
//}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [newList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([OrderCell class]) forIndexPath:indexPath];
    [cell configure:newList[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.section == 0){
        //newList[indexPath.row]
        ViewController *detail = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass([ViewController class])];
        
        detail.order = newList[indexPath.row];
        [self.navigationController pushViewController:detail animated:YES];
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
