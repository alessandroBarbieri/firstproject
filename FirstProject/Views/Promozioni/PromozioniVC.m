//
//  PromozioniVC.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "PromozioniVC.h"
#import "PromotionCell.h"

@interface PromozioniVC ()

@end

@implementation PromozioniVC{
    NSArray* newList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    newList = @[
                @{
                    @"PromotionTitle": @"PAZZESCO! 50+40% di sconto!",
                    @"PromotionInfo": @"Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    @"PromotionTime": @"Promo valida fino al: 31/01/2018"
                    },
                @{
                    @"PromotionTitle": @"SALDI BRC: -30% su tutto!",
                    @"PromotionInfo": @"Batterie, filtri e refrigeranti, per tutto Gennaio 2018!",
                    @"PromotionTime": @"Promo valida fino al: 23/02/2018"
                    },
                @{
                    @"PromotionTitle": @"Novità nel catalogo BRC",
                    @"PromotionInfo": @"Scopri i nuovi filtri per GPL e auto a metano BRC...",
                    @"PromotionTime": @"Promo valida fino al: 10/05/2017"
                    },
                @{
                    @"PromotionTitle": @"BLACK FRIDAY: prezzi sbalorditivi!",
                    @"PromotionInfo": @"Oli e lubrificanti super scontati con 3 pezzi 1 omaggio!",
                    @"PromotionTime": @"Promo valida fino al: 30/05/2018"
                    }
                ];
    
    [self.promozioniTableView registerNib:[UINib nibWithNibName:NSStringFromClass([PromotionCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PromotionCell class])];
    
    self.promozioniTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [newList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PromotionCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PromotionCell class]) forIndexPath:indexPath];
    [cell configure:newList[indexPath.row]];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
