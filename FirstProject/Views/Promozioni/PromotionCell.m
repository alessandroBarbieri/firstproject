//
//  PromotionCell.m
//  FirstProject
//
//  Created by Etinet on 09/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "PromotionCell.h"

@implementation PromotionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configure:(NSDictionary*)elem {
    self.promotionTitle.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"PromotionTitle"]];
    self.promotionDescription.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"PromotionInfo"]];
    self.promotionTime.text = [NSString stringWithFormat:@"%@",[elem objectForKey:@"PromotionTime"]];
}

@end
