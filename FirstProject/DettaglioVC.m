//
//  DettaglioVC.m
//  FirstProject
//
//  Created by Etinet on 07/05/18.
//  Copyright © 2018 DigitalX srl. All rights reserved.
//

#import "DettaglioVC.h"
#import "DetailCell.h"
#import "infoCell.h"
#import "PriceCell.h"


@interface DettaglioVC ()

@end

@implementation DettaglioVC

@synthesize product;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //NSLog([product objectForKey:@"NomeProdotto"]);
    [self.detailTableView registerNib:[UINib nibWithNibName:NSStringFromClass([DetailCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([DetailCell class])];
    
    [self.detailTableView registerNib:[UINib nibWithNibName:NSStringFromClass([infoCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([infoCell class])];
    
    [self.detailTableView registerNib:[UINib nibWithNibName:NSStringFromClass([PriceCell class ]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([PriceCell class])];
    
    self.detailTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.detailTableView.estimatedRowHeight = 474;
    
    
    //if(from carrello){
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preferiti_outline_icon-1"] style:UIBarButtonItemStyleDone target:self action:@selector(onPreferito:)];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.97 green:0.76 blue:0.33 alpha:1.0]];
    //}
    
}

-(IBAction)onPreferito:(id)sender{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preferiti_full_icon"] style:UIBarButtonItemStyleDone target:self action:@selector(onRimuoviPreferito:)];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.97 green:0.76 blue:0.33 alpha:1.0]];
}

-(IBAction)onRimuoviPreferito:(id)sender{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"preferiti_outline_icon-1"] style:UIBarButtonItemStyleDone target:self action:@selector(onPreferito:)];
    [self.navigationItem.rightBarButtonItem setTintColor:[UIColor colorWithRed:0.97 green:0.76 blue:0.33 alpha:1.0]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    self.title = @"DETTAGLIO PRODOTTO";
   
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    } else if (section == 1) {
        return 6;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section == 0) {
        //tipo uno di cella
        DetailCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([DetailCell class]) forIndexPath:indexPath];
        [cell configure: product];
        return cell;
    } else if(indexPath.section == 1) {
        infoCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([infoCell class]) forIndexPath:indexPath];
        [cell configure: product index:indexPath.row];
        //NSLog(@"RICARICO CELLAAAAAAAA!!!!!!!!!");
        return cell;
    } else {
        PriceCell* cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([PriceCell class]) forIndexPath:indexPath];
        [cell configure: product];
        cell.delegate = self;
        //NSLog(@"RICARICO CELLAAAAAAAA!!!!!!!!!");
        return cell;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0) {
        return 474;
    }else if(indexPath.section == 1) {
        return 44;
    } else {
        return 158;
    }
}

/*-(void)updateTable {
    [self.detailTableView reloadData];
    NSLog(@"RICARICO TABELLAAAAAAAA!!!!!!!!!");
}*/

#pragma delegate

-(void)priceUpdated:(double)quantity{
    //NSLog(@"RICARICO TABELLAAAAAAAA!!!!!!!!!");
    
    NSMutableDictionary * _mut = product.mutableCopy;
    [_mut setObject:@(quantity) forKey:@"QtaOrder"];
    product = [[NSDictionary alloc] initWithDictionary:_mut];
    [self.detailTableView reloadData];

    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
